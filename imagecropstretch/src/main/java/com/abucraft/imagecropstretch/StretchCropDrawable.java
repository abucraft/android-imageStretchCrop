package com.abucraft.imagecropstretch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/**
 * Created by c on 2015/8/2.
 */
public class StretchCropDrawable extends Drawable {
    Point ltPoint=new Point();
    Point lbPoint=new Point();
    Point rtPoint=new Point();
    Point rbPoint=new Point();
    Paint mPaint=new Paint();
    public final static int RADIUS=5;
    Context mContext;

    public StretchCropDrawable(Context context) {
        mContext=context;
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(2f);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);

    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        ltPoint.set((right - left) / 4 + left, (bottom - top) / 4 + top);
        lbPoint.set((right - left) / 4 + left, 3 * (bottom - top) / 4 + top);
        rtPoint.set(3 * (right - left) / 4 + left, (bottom - top) / 4 + top);
        rbPoint.set(3 * (right - left) / 4 + left, 3 * (bottom - top) / 4 + top);
    }

    @Override
    public void setBounds(Rect bounds) {
        super.setBounds(bounds);
        ltPoint.set(bounds.width() / 4+bounds.left, bounds.height() / 4+bounds.top);
        lbPoint.set(bounds.width() / 4+bounds.left, 3 * bounds.height() / 4+bounds.top);
        rtPoint.set(3 * bounds.width() / 4+bounds.left, bounds.height() / 4+bounds.top);
        rbPoint.set(3*bounds.width() /4+bounds.left,3*bounds.height()/4+bounds.top);
    }

    public void mapPoints(Point nltPoint,Point nlbPoint,Point nrbPoint,Point nrtPoint){
        nltPoint.set(ltPoint.x,ltPoint.y);
        nlbPoint.set(lbPoint.x,lbPoint.y);
        nrbPoint.set(rbPoint.x,rbPoint.y);
        nrtPoint.set(rtPoint.x,rtPoint.y);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(ltPoint.x,ltPoint.y,dpTopx(mContext,RADIUS),mPaint);
        canvas.drawCircle(lbPoint.x,lbPoint.y,dpTopx(mContext,RADIUS),mPaint);
        canvas.drawCircle(rtPoint.x, rtPoint.y, dpTopx(mContext, RADIUS), mPaint);
        canvas.drawCircle(rbPoint.x, rbPoint.y, dpTopx(mContext, RADIUS), mPaint);
        canvas.drawLine(ltPoint.x,ltPoint.y,rtPoint.x,rtPoint.y,mPaint);
        canvas.drawLine(rtPoint.x,rtPoint.y,rbPoint.x,rbPoint.y,mPaint);
        canvas.drawLine(rbPoint.x,rbPoint.y,lbPoint.x,lbPoint.y,mPaint);
        canvas.drawLine(lbPoint.x,lbPoint.y,ltPoint.x,ltPoint.y,mPaint);
    }
    public void setLtPoint(int x,int y){
        ltPoint.set(x,y);
    }
    public void setLbPoint(int x,int y){
        lbPoint.set(x,y);
    }
    public void setRtPoint(int x,int y){
        rtPoint.set(x,y);
    }
    public void setRbPoint(int x,int y){
        rbPoint.set(x,y);
    }

    public int dpTopx(Context context,int dp){
        final float scale=context.getResources().getDisplayMetrics().density;
        return (int)(dp*scale);
    }
}
