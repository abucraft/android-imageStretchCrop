package com.abucraft.imagecropstretch;

import android.graphics.Point;

/**
 * Created by c on 2015/8/4.
 */
public class StretchBitmap {
    public static boolean isConvexQuadrangle(Point ltPoint,Point lbPoint,Point rbPoint,Point rtPoint){
        if(ltPoint.equals(lbPoint)||ltPoint.equals(rbPoint)||ltPoint.equals(rtPoint)||
                lbPoint.equals(rbPoint)||lbPoint.equals(rtPoint)||rbPoint.equals(rtPoint)) {
            return false;
        }

        if(intersect(ltPoint,rbPoint,lbPoint,rtPoint)){
            return true;
        }
        return false;
    }

    //判断两个线段是否相交，如果某个点在线段上，返回false
    protected static boolean intersect(Point a,Point b,Point c,Point d){
        if ( Math.max(a.x, b.x)<Math.min(c.x, d.x) )
        {
            return false;
        }
        if ( Math.max(a.y, b.y)<Math.min(c.y, d.y) )
        {
            return false;
        }
        if ( Math.max(c.x, d.x)<Math.min(a.x, b.x) )
        {
            return false;
        }
        if ( Math.max(c.y, d.y)<Math.min(a.y, b.y) )
        {
            return false;
        }
        if(crossProduct(a,c,d)*crossProduct(b,c,d)>=0){
            return false;
        }
        if(crossProduct(c,a,b)*crossProduct(d,a,b)>=0){
            return false;
        }
        return true;
    }
    protected static int crossProduct(Point a,Point b,Point c){
        return (b.x-a.x)*(c.y-a.y)-(b.y-a.y)*(c.x-a.x);
    }
}
