package com.abucraft.sample;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.abucraft.imagecropstretch.CropImageActivity;

import java.io.File;


public class MainActivity extends ActionBarActivity {

    private final static int GET_IMAGE_FROM_SYSTEM=1;
    private ImageView resultImage;
    private TextView resultText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultImage=(ImageView)findViewById(R.id.imageView);
        resultText=(TextView)findViewById(R.id.textView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_select) {
            Intent pickImageIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
            pickImageIntent.setType("image/*");
            startActivityForResult(pickImageIntent, GET_IMAGE_FROM_SYSTEM);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void beginCrop(Uri src){
        Uri dest=Uri.fromFile(new File(getCacheDir(),"cropped"));
        Intent intent=new Intent(this, CropImageActivity.class);
        intent.setData(src);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,dest);
        intent.putExtra(CropImageActivity.REQUEST,CropImageActivity.REQ_CROP_BASE);
        startActivityForResult(intent, CropImageActivity.REQ_CROP_BASE);
        return;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CropImageActivity.REQ_CROP_BASE){
            if(resultCode==CropImageActivity.RESULT_OK){
                Uri dest=data.getParcelableExtra(MediaStore.EXTRA_OUTPUT);
                resultImage.setImageURI(null);
                resultImage.setImageURI(dest);
                resultImage.invalidate();
            }
        }
        if(requestCode==GET_IMAGE_FROM_SYSTEM){
            if(data!=null){
                beginCrop(data.getData());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
